package com.panisa.midterm2;

import java.util.Scanner;

public class KanomShop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Enter Name : ");
            String name = sc.nextLine();

            System.out.print("Kanomtian type : ");
            int type = sc.nextInt();
            System.out.print("Unit : ");
            int unit1 = sc.nextInt();

            System.out.print("Kanomkeng : ");
            int unit2 = sc.nextInt();

            Customer customer = new Customer(name, new Kanomtian(type,unit1), new Kanomkeng(unit2));
            customer.print();
        }
        
    }   
    
}
