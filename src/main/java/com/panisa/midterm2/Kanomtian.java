package com.panisa.midterm2;

public class Kanomtian {
    private String type;
    private int price = 100;
    private int number;

    public Kanomtian(int type, int number) {
        if(type==0) {
            this.type = "Sweet"; //ขนมเทียนไส้หวาน
        } else if(type==1) {
            this.type = "Salty"; //ขนมเทียนไส้เค็ม
        } else {
            this.type = "None";
        }
    }

    public Kanomtian() {
        this.type = "None";
        this.number = 0;
    }

    public void setType(String type) {
        if(type.length() == 0) {
            return;
        }
        this.type = type;
    }
    
    public void setNumber(int number) {
        this.number = number;
    }

    public int gettotalPrice(int price) {
        return this.price*this.number;
    }

    public String toString() {
        return String.format("Kanomtian (%s) : %d unit price = %d", type, number, gettotalPrice(price));
    }
}
