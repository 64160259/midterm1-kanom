package com.panisa.midterm2;

public class Customer {
    private String name;
    private Kanomtian k1;
    private Kanomkeng k2;

    public Customer(String name) {
        this.name = name;
        this.k1 = null;
        this.k2 = null;
    }

    public Customer(String name, Kanomtian k1, Kanomkeng k2) {
        this(name);
        this.k1 = k1;
        this.k2 = k2;
    }

    public void print() {
        System.out.print("Name : " + this.name);
        int total = 0;
        if(this.k1 != null) {
            System.out.println(this.k1);
        } else if(this.k2 != null) {
            System.out.println(this.k2);
        } else 
    
        System.out.print("Total price : "+ (this.k1.gettotalPrice(total) + this.k2.gettotalPrice()));
    }
    
}
