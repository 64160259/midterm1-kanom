package com.panisa.midterm2;

public class Kanomkeng {
    private int price = 150;
    private int number;

    public Kanomkeng(int number) {
        this.number = validation(number);
    }

    public int validation(int number) {
        return number;
    } 

    public int gettotalPrice() {
        return this.price * this.number;
    }

    public String toString() {
        return String.format("Kanomkeng : %d unit price = ", this.number, this.gettotalPrice());
    }
    
}
